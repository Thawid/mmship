<?php
require_once '../config.php';
require_once '../functions.php';
$categoryname = isset($_POST['categoryname']) ? $_POST['categoryname'] : null;
$start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
$end_date = isset($_POST['end_date']) ? $_POST['end_date'] : null;

$dateOne = date_create("$start_date");
$date_one =  date_format($dateOne,"jS F Y");

$dateTwo = date_create("$end_date");
$date_two = date_format($dateTwo,"jS F Y");

echo "<p><em> <strong>{$date_one}</strong> থেকে  <strong>{$date_two}</strong>  পর্যন্ত  রিপোর্ট </em></p>";

$response = '<div class="table-responsive mb-4 mt-4">';
$response .= '<table id="zero-config" class="table table-hover dataTable" style="width: 100%;" role="grid" aria-describedby="zero-config_info">
<thead>
<tr>
    <th>তারিখ </th>
    <th> Category </th>
    <th> মোট বিক্রি টাকার পরিমান</th>
    <th>বিস্তারিত</th>

</tr>
</thead>';
$response .= '<tbody>';
$result = nagadReport($categoryname,$start_date,$end_date);
while ($rows = mysqli_fetch_assoc($result)) {
    $nagad_date = $rows['nagad_date'];
    $timestamp = strtotime($nagad_date);
    $date =  date('d/m/Y', $timestamp);
    $categoryname = $rows['categoryname'];
    $nagad_amount = bn(number_format($rows['nagad_amount']));
    $nagad_details = $rows['nagad_details'];
    $totalSale += $rows['nagad_amount'];
    $total_sale = bn(number_format($totalSale));

    $response .= '<tr>';
    $response .= '<td>' . "$date" . '</td>';
    $response .= '<td>' . "$categoryname" . '</td>';
    $response .= '<td>' . "$nagad_amount" . '</td>';
    $response .= '<td>' . "$nagad_details" . '</td>';
    $response .= '</tr>';
}
$response .= '</tbody>';
$response .= '<tfoot>';
$response .= '<tr>';
$response .= '<th></th>';
$response .= '<th></th>';
$response .= '<th>মোট :' . "$total_sale" . '/=</th>';
$response .= '<th></th>';
$response .= '</tr>';
$response .= '</tfoot>';
$response .= '</table>';
$response .= '</div>';
$response .= '<div class="row">
                            <div class="col-12">
                                <input type="button" id="print" value="Invoice" class="btn btn-primary mb-2"/>
                            </div>
                        </div>';

echo $response;
?>
<script>
    $('#zero-config').DataTable({
        "oLanguage": {
            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [7, 10, 20, 50],
        "pageLength": 7
    });

    function printDataUsingjQuery(){
        let params = {
            "categoryname":$("#categoryname").val(),
            "start_date":$("#basicFlatpickr").val(),
            "end_date":$("#basicFlatpickr1").val(),
        }

        $.ajax({
            "method":"POST",
            "url":"ajax/PrintNagadReport.php",
            "data":params
        }).done(function(response){
            $("#result").html(response);
        });
        return false;
    }

    document.getElementById("print").addEventListener("click", function() {
        printDataUsingjQuery();
    });
</script>
