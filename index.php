<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include "header.php";
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
mysqli_set_charset($connection, "utf8");
if (!$connection) {
    throw new Exception("Cannot connect to database");
}


?>
			<!-- Start Content -->
			<div class="layout-px-spacing">
				<!-- Start breadcrumb -->
				<div class="page-header">
					<div class="page-title">
						<h3>ড্যাশবোর্ড</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>

							<li class="breadcrumb-item active" aria-current="page"><span>ড্যাশবোর্ড</span></li>
						</ol>
					</nav>
				</div>
				<!-- End breadcrumb -->
                <!-- CONTENT AREA -->
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
								<div class="row">
                                    <div class="col-lg-4">
                                        <?php
                                            $totdayNagad = todayNagad();
                                            $nagad = mysqli_fetch_assoc($totdayNagad);
                                        ?>
										<button class="btn btn-primary mb-4  btn-block btn-lg">
											<b> আজকের নগদ   </b> </br> <?php echo bn(number_format((float)$nagad['amount'])); ?> /=
										</button>
                                    </div>
                                    <div class="col-lg-4">
                                        <?php
                                            $todayExpense = todayExpense();
                                            $expense = mysqli_fetch_assoc($todayExpense);
                                        ?>
										<button class="btn btn-success mb-4  btn-block btn-lg">
											<b> আজকের খরচ </b> </br> <?php echo bn(number_format((float)$expense['expense'])); ?> /=
										</button>
                                    </div>
                                    <div class="col-lg-4">
										<button class="btn btn-warning mb-4  btn-block btn-lg">
											<b>আজকের একাউন্টে জমা</b> </br> <?php echo bn(number_format((float)$nagad['amount'] - $expense['expense'])); ?> /=
										</button>
                                    </div>
                                    
                                </div>
								
								<div class="row">
                                    <div class="col-lg-4">
                                        <?php
                                            $monthlyNagad = monthlyNagad();
                                            $monthly_nagad = mysqli_fetch_assoc($monthlyNagad);
                                        ?>
										<button class="btn btn-primary mb-4  btn-block btn-lg">
											<b> চলতি মাসের নগদ </b> </br> <?php echo bn(number_format((float)$monthly_nagad['monthly_nagad'])); ?> /=
										</button>
                                    </div>
                                    <div class="col-lg-4">
                                    <?php
                                        $monthlyExpense = monthlyExpense();
                                        $monthly_expense = mysqli_fetch_assoc($monthlyExpense);
                                    ?>
										<button class="btn btn-success mb-4  btn-block btn-lg">
											<b> চলতি মাসের খরচ</b> </br> <?php echo bn(number_format((float)$monthly_expense['monthly_expense'])); ?> /=
										</button>
                                    </div>
                                    <div class="col-lg-4">
										<button class="btn btn-warning mb-4  btn-block btn-lg">
											<b>চলতি মাসের একাউন্টে জমা</b> </br> <?php echo bn(number_format((float)$monthly_nagad['monthly_nagad'] - $monthly_expense['monthly_expense'])); ?> /=
										</button>
                                    </div>
                                   
                                </div>
								
								<div class="row">
                                    <div class="col-lg-4">
                                    <?php
                                        $yearlyNagad = yearlyNagad();
                                        $yearly_nagad = mysqli_fetch_assoc($yearlyNagad);
                                    ?>
										<!--<button class="btn btn-primary mb-4  btn-block btn-lg">
											<b>বছরের সর্বমোট নগদ </b> </br> <?php /*echo bn(number_format((float)$yearly_nagad['yearly_nagad'])); */?> /=
										</button>-->
                                    </div>
                                    <div class="col-lg-4">
                                    <?php
                                        $yearlyExpense = yearlyExpense();
                                        $yearly_expense = mysqli_fetch_assoc($yearlyExpense);
                                    ?>
										<!--<button class="btn btn-success mb-4  btn-block btn-lg">
											<b>বছরের সর্বমোট খরচ</b> </br> <?php /*echo bn(number_format((float)$yearly_expense['yearly_expense'])); */?> /=
										</button>-->
                                    </div>
                                    <div class="col-lg-4">
										<!--<button class="btn btn-warning mb-4  btn-block btn-lg">
											<b> একাউন্টে জমা</b> </br> <?php /*echo bn(number_format((float)$yearly_nagad['yearly_nagad'] - $yearly_expense['yearly_expense'])); */?> /=
										</button>-->
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <?php
                                        $bankDeposited = bankDeposited();
                                        $bank_deposited = mysqli_fetch_assoc($bankDeposited);
                                        $withdraw = bankWithDraw();
                                        $bankWithdraw = mysqli_fetch_assoc($withdraw);
                                        $bankSave = $bank_deposited['depisoted'] - $bankWithdraw['withdraw'];
                                        $deposited = $bank_deposited['depisoted'];
                                        $Expense = $yearly_expense['yearly_expense'];
                                        $total_expense= $deposited + $Expense;
                                        $bank_withdraw = $bankWithdraw['withdraw'];
                                        $cash = ($yearly_nagad['yearly_nagad'] - $total_expense) + $bank_withdraw;
                                        $finalCash = $cash + $bankSave ;

                                        ?>
                                        <button class="btn btn-primary mb-4  btn-block btn-lg">

                                            <b>দোকানের ক্যাশ  </b> </br> <?php echo  bn(number_format((float)$cash)); ?> /=
                                        </button>
                                    </div>
                                    <div class="col-lg-4">

                                        <button class="btn btn-success mb-4  btn-block btn-lg">
                                            <b>ব্যাংক জমা</b> </br> <?php echo bn(number_format((float)$bankSave)); ?> /=
                                        </button>
                                    </div>
                                    <div class="col-lg-4">
                                        <button class="btn btn-warning mb-4  btn-block btn-lg">
                                            <b> একাউন্টে জমা</b> </br> <?php echo  bn(number_format((float)$finalCash)); ?> /=
                                        </button>
                                    </div>

                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End Content -->
<?php include "footer.php"; ?>
          