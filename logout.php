<?php
session_start();
$_SESSION['id'] = 0;
$_SESSION['login'] = false;
$_SESSION['user_type'] = false;
session_destroy();
header('Location: auth.php');