<?php
    session_start();
    require_once 'functions.php';
    Authorization();
    include_once 'config.php';
    include "header.php";
    $action = $_POST['action'] ?? '';
    $task = $_GET['task'] ?? '';
    $status = 0;

    if ('addSalary' == $action) {
        $staff_id = filter_input(INPUT_POST, 'staff_id', FILTER_SANITIZE_STRING);
        $salary_amount = filter_input(INPUT_POST, 'salary_amount', FILTER_SANITIZE_STRING);
        $salary_date = filter_input(INPUT_POST, 'salary_date', FILTER_SANITIZE_STRING);
        $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);
        addStaffSalary($staff_id, $salary_amount, $salary_date, $details);
    }

if ('updateSalary' == $action) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $staff_id = filter_input(INPUT_POST, 'staff_id', FILTER_SANITIZE_STRING);
    $salary_amount = filter_input(INPUT_POST, 'salary_amount', FILTER_SANITIZE_STRING);
    $salary_date = filter_input(INPUT_POST, 'salary_date', FILTER_SANITIZE_STRING);
    $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);

    updateSalary($id, $staff_id, $salary_amount, $salary_date, $details);
}

    if ('delete' == $task) {
        $id = $_GET['id'];
        deleteStaffSalary($id);
    }
?>
			<!-- Start Content -->
			<div class="layout-px-spacing">
				<!-- Start breadcrumb -->
				<div class="page-header">
					<div class="page-title">
						<h3>কর্মচারীর বেতন এর হিসেবে তথ্য</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
							<li class="breadcrumb-item active" aria-current="page"><span>কর্মচারীর বেতন</span></li>
						</ol>
					</nav>
				</div>
				<!-- End breadcrumb -->
                <!-- CONTENT AREA -->
                <?php
                $status = $_GET['status']??0;
                if(20 == $status){   ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-info mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(21 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(21 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(22 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-warning mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } elseif(33 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-success mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="row layout-top-spacing">
                    <div class="col-4 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
								<h5 class="text-center">নতুন বেতন তথ্য</h5>
                                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
									<div class="form-group mb-4">
										<label for="usernameb">কর্মচারীর নাম</label><br>
										<select class="form-control selectpicker" id="staff_id" name="staff_id" data-live-search="true">
											<option value="">Select Staff</option>
                                            <?php
                                                $result = getAllEmployee();
                                                while ($rows = mysqli_fetch_assoc($result)) {
                                            ?>
                                            <option value="<?php echo $rows['id']; ?>"><?php echo $rows['staff_name']; ?><??></option>
                                            <?php } ?>
										</select>
									</div>
									<div class="form-group mb-4">
										<label for="takatotla">টাকার পরিমান </label>
										<input id="takatotla" type="number" name="salary_amount" placeholder="২০,২৫০ টাকা " class="form-control" required="">
									</div>
									<div class="form-group mb-4">
										<label for="dateb">তারিখ </label>
										<input id="basicFlatpickr" name="salary_date" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Select Date.." readonly="readonly">
									</div>
									<div class="form-group mb-4">
										<label for="Detailsb">বিস্তারিত </label>
										<textarea class="form-control" id="details" name="details" rows="2"></textarea>
									</div>
                                    <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                    <input type="hidden" name="action" id="action" value="addSalary">
								</form>
                            </div>
                        </div>
                    </div>
					<div class="col-8 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
							<h5 class="text-center">কর্মচারীর বেতন তথ্য  তালিকা</h5>
                            <div class="table-responsive mb-4">
                                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                    <thead>
                                        <tr>
											<th>সিরিয়াল</th>
                                            <th>কর্মচারীর নাম</th>
                                            <th>তারিখ</th>
                                            <th> টাকার পরিমান</th>
											<th>বিস্তারিত</th>
											<th>অ্যাকশান</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $result = getAllSalary();
                                    $count = 1;
                                    while ($rows = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $rows['staff_name']; ?></td>
                                            <td><?php echo $rows['salary_date']; ?></td>
                                            <td><?php echo bn(number_format($rows['salary_amount'])); ?></td>
                                            <td><?php echo $rows['details']; ?></td>
                                            <td>
                                                <a type="button" class="" data-toggle="modal" data-target="#salaryUpdate<?php echo $rows['id'];?>">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit-3">
                                                        <path d="M12 20h9"></path>
                                                        <path
                                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                    </svg>
                                                </a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="salaryUpdate<?php echo $rows['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle"> বেতন তথ্য</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="widget-one">
                                                            <?php //echo $rows['staff_id']; ?>
                                                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                                                                <div class="form-group mb-4">
                                                                    <label for="usernameb">কর্মচারীর নাম</label><br>
                                                                    <select name="staff_id" id="staff_id" class="form-control">
                                                                        <?php
                                                                        $empList = employeeList();
                                                                        while ($emp = mysqli_fetch_assoc($empList)){ ?>
                                                                            <option value="<?php echo $emp['id'];?>" <?php if($emp['id'] == $rows['staff_id']){ echo 'selected';}?>><?php echo $emp['staff_name'];?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group mb-4">
                                                                    <label for="takatotla">টাকার পরিমান </label>
                                                                    <input id="takatotla" type="number" name="salary_amount" value="<?php echo $rows['salary_amount']; ?>" class="form-control" required="">
                                                                </div>
                                                                <div class="form-group mb-4">
                                                                    <label for="dateb">তারিখ </label>
                                                                    <input type="date" id="salary_date" name="salary_date"
                                                                           class="form-control flatpickr flatpickr-input active"
                                                                           value="<?php echo $rows['salary_date']; ?>">
                                                                </div>
                                                                <div class="form-group mb-4">
                                                                    <label for="Detailsb">বিস্তারিত </label>
                                                                    <textarea class="form-control" id="details" name="details" rows="2"><?php echo $rows['details']; ?></textarea>
                                                                </div>

                                                                <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                                                <input type="hidden" name="id"  value="<?php echo $rows['id']; ?>">
                                                                <input type="hidden" name="action" id="action" value="updateSalary">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $count++;
                                    }
                                    ?>
                                     </tbody>
                                </table>
                            </div>
                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End Content -->
<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>