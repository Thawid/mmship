<?php
    session_start();
    require_once 'functions.php';
    Authorization();
    include_once 'config.php';
    include "header.php";
    $action = $_POST['action'] ?? '';
    $task = $_GET['task'] ?? '';
    $status = 0;

    if ('addSale' == $action) {
        //$departmentName = $_POST['departmentName'];
        $sale_amount = filter_input(INPUT_POST, 'sale_amount', FILTER_SANITIZE_STRING);
        $sale_date = filter_input(INPUT_POST, 'sale_date', FILTER_SANITIZE_STRING);
        $sale_details = filter_input(INPUT_POST, 'sale_details', FILTER_SANITIZE_STRING);
        addSale($sale_amount, $sale_date, $sale_details);
    }

    if ('delete' == $task) {
        $id = $_GET['id'];
        deleteSale($id);
    }
?>

			<!-- Start Content -->
			<div class="layout-px-spacing">
				<!-- Start breadcrumb -->
				<div class="page-header">
					<div class="page-title">
						<h3>আজকের বিক্রি তথ্যসমূহ</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
							<li class="breadcrumb-item active" aria-current="page"><span>খরচ</span></li>
						</ol>
					</nav>
				</div>
				<!-- End breadcrumb -->
                <?php
                $status = $_GET['status']??0;
                if(6 == $status){   ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-info mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } elseif(7 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(8 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-success mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Success !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- CONTENT AREA -->
                <div class="row layout-top-spacing">
                    <div class="col-4 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
								<h5 class="text-center">নতুন বিক্রি তথ্য</h5>
								<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
									
									<div class="form-group mb-4">
										<label for="takatotla">টাকার পরিমান </label>
										<input id="sale_ammount" type="number" name="sale_amount" placeholder="২০,২৫০ টাকা " class="form-control" required="">
									</div>
									<div class="form-group mb-4">
										<label for="dateb">তারিখ </label>
										<input type="date" id="" name="sale_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" type="text" placeholder="Select Date..">
									</div>
									<div class="form-group mb-4">
										<label for="Detailsb">বিস্তারিত </label>
										<textarea class="form-control" id="sale_details" name="sale_details" rows="1"></textarea>
									</div>
									<input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                    <input type="hidden" name="action" id="action" value="addSale">
								</form>
                            </div>
                        </div>
                    </div>
					<div class="col-8 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
							<h5 class="text-center">বিক্রি তালিকা</h5>
                            <div class="table-responsive mb-4">
                                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                    <thead>
                                        <tr>
											<th>সিরিয়াল</th>
                                            <th>আজকের বিক্রি টাকার পরিমান</th>
                                            <th>তারিখ</th>
											<th>বিস্তারিত</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $result = getAllSale();
                                    $count = 1;
                                    while ($rows = mysqli_fetch_assoc($result)) {  ?>
                                        <tr>
											<td><?php echo $count; ?></td>
                                            <td><?php echo bn(number_format($rows['sale_amount'])); ?></td>
                                            <td><?php echo $rows['sale_date']; ?></td>
                                            <td><?php echo $rows['sale_details']; ?></td>
                                            <td><?php printf("<a class='delete' href='TodaySale.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>",$rows['id']) ?></td>

                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    ?>
                                     </tbody>
                                </table>
                            </div>
                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End Content -->
<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>
          