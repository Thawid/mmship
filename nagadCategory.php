<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include "header.php";
$action = $_POST['action'] ?? '';
$task = $_GET['task'] ?? '';
$status = 0;

if ('addCategory' == $action) {
    $category_name = filter_input(INPUT_POST, 'category_name', FILTER_SANITIZE_STRING);
    $category_details = filter_input(INPUT_POST, 'category_details', FILTER_SANITIZE_STRING);
    addNagadCategory($category_name, $category_details);
}

if ('delete' == $task) {
    $id = $_GET['id'];
    deleteNagadCategory($id);
}
?>
<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>নগদের ক্যাটাগরি</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                <li class="breadcrumb-item active" aria-current="page"><span>ক্যাটাগরি</span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <?php
    $status = $_GET['status']??0;
    if(34 == $status){   ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(35 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(36 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Success !!  </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(37 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row layout-top-spacing">
        <div class="col-4 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন নগদের  ক্যাটাগরি </h5>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                        <div class="form-group mb-4">
                            <label for="categoryName">ক্যাটাগরি নাম </label>
                            <input id="categoryName" type="text" name="category_name" placeholder="ক্যাটাগরি নাম " class="form-control" required="">
                        </div>
                        <div class="form-group mb-4">
                            <label for="Detailsb">বিস্তারিত </label>
                            <textarea class="form-control" id="category_details" name="category_details" rows="2"></textarea>
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="action" id="action" value="addCategory">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">ক্যাটাগরি তথ্য  তালিকা</h5>
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>সিরিয়াল</th>
                                <th>ক্যাটাগরি নাম  </th>
                                <th>বিস্তারিত </th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $result = getNagadCategory();
                            $count = 1;
                            while ($rows = mysqli_fetch_assoc($result)) {
                               ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $rows['categoryname']; ?></td>
                                    <td><?php echo $rows['description']; ?></td>
                                    <td><?php printf("<a class='delete' href='nagadCategory.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>",$rows['id']) ?></td>
                                </tr>
                                    <?php
                                    $count++;
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->
<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>
