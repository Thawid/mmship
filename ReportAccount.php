<?php
    session_start();
    require_once 'functions.php';
    Authorization();
    include_once 'config.php';
    include "header.php";
    $action = $_POST['action'] ?? '';

?>
        <!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content" xmlns="http://www.w3.org/1999/html">
    <div class="layout-px-spacing">
        <div class="page-header">
            <div class="page-title">
                <h3> একাউন্ট রিপোর্ট  </h3>
            </div>
            <nav class="breadcrumb-one" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item"><a href="#">রিপোর্ট</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><span>একাউন্ট রিপোর্ট  </span>
                    </li>
                </ol>
            </nav>
        </div>
        <div id="content" class="main-content">

            <div class="row layout-top-spacing" id="cancel-row">

                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div class="widget-content widget-content-area br-6">
                        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                            <div class="row">
                                <div class="col-lg-4">
                                    <p><b>শুরু তারিখ</b></p>
                                    <div class="form-group">
                                        <input id="basicFlatpickr" name="start_date"
                                               class="form-control flatpickr flatpickr-input active" type="text"
                                               placeholder="শুরু তারিখ.." readonly="readonly" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <p><b>শেষের তারিখ</b></p>
                                    <input id="basicFlatpickr1" name="end_date"
                                           class="form-control flatpickr flatpickr-input active" type="text"
                                           placeholder="শেষের তারিখ.." readonly="readonly" required>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <p><b> তথ্যের জন্য নিচের বাটনে ক্লিক করুন</b></p>
                                    <button type="button" id="send"   class="btn btn-success btn-lg btn-block mb-2"/>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                         stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-search">
                                        <circle cx="11" cy="11" r="8"></circle>
                                        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                    </svg>
                                    অনুসন্ধান
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div id="result"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>
<script>

    function sendDataUsingjQuery(){
        let params = {
            "start_date":$("#basicFlatpickr").val(),
            "end_date":$("#basicFlatpickr1").val(),
        }

        $.ajax({
            "method":"POST",
            "url":"ajax/GetAccountReport.php",
            "data":params
        }).done(function(response){
            $("#result").html(response);
        });
        return false;
    }

    document.getElementById("send").addEventListener("click", function() {
        sendDataUsingjQuery();
    });
</script>
